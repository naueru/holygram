﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    public float moveSpeed = 0.1F;
    public Rigidbody rb;

    private PlayerStates states;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        states = GetComponent<PlayerStates>();
        initLvl();
    }

    void initLvl () {
        states = GetComponent<PlayerStates>();
        gameObject.transform.position = states.getInitLvlPos();
        levelState lvlState = states.getLevel().GetComponent<levelState>();
        states.activeCamera = lvlState.initSide;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonUp("Jump")) {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        }

        if (Input.GetButtonUp("Fire2")) {
            states.activeCamera ++;
            if (states.activeCamera > 3) {
                states.activeCamera = 0;
            }
        }

        if (Input.GetButtonUp("Fire1")) {
            states.activeCamera --;
            if (states.activeCamera < 0) {
                states.activeCamera = 3;
            }
        }

        var vel = new Vector3(0,0,0);

        switch (states.activeCamera) {
            case 0:
                vel = new Vector3(-Input.GetAxis("Horizontal") * moveSpeed, rb.velocity.y, rb.velocity.z);
                states.activeScreen.transform.SetPositionAndRotation(new Vector3(0, 3, 6), Quaternion.Euler(0, 0, 0));
                break;
            case 1:
                vel = new Vector3(rb.velocity.x, rb.velocity.y, Input.GetAxis("Horizontal") * moveSpeed);
                states.activeScreen.transform.SetPositionAndRotation(new Vector3(6, 3, 0), Quaternion.Euler(0, 90, 0));
                break;
            case 2:
                vel = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, rb.velocity.y, rb.velocity.z);
                states.activeScreen.transform.SetPositionAndRotation(new Vector3(0, 3, -6), Quaternion.Euler(0, 180, 0));
                break;
            case 3:
                vel = new Vector3(rb.velocity.x, rb.velocity.y, -Input.GetAxis("Horizontal") * moveSpeed);
                states.activeScreen.transform.SetPositionAndRotation(new Vector3(-6, 3, 0), Quaternion.Euler(0, 270, 0));
                break;
        }

        rb.velocity = vel;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Death") {
            states.lives --;
            if ( states.lives <= 0) {
                states.isAlive = false;
                states.reset();
                UnityEditor.EditorUtility.DisplayDialog ("Moritste putooo", "Moriste Puto", "Ok");
            } 
            initLvl();
        }

        if(collision.gameObject.tag == "Finish") {
            levelState lvlState = states.getLevel().GetComponent<levelState>();
            states.currentLvlIndex = lvlState.toLevelIndex;
            lvlState.hide();
            levelState nextLvl = states.getLevel().GetComponent<levelState>();
            nextLvl.show();
            initLvl();
            UnityEditor.EditorUtility.DisplayDialog ("Ganaste", "Ganaste", "Ok");
        }
    }
}
