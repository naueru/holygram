﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStates : MonoBehaviour
{
    public int currentLvlIndex = 0;
    public GameObject currentLevel;
    public int activeCamera = 0;
    public GameObject activeScreen;
    public int lives = 3;
    public bool isAlive = true;
    public bool showMenu = true;

    public void reset() {
        currentLvlIndex = 0;
        activeCamera = 0;
        lives = 3;
        isAlive = true;
        showMenu = true;
    }

    public GameObject getLevel() {
        GameObject[] levels;
        levels = GameObject.FindGameObjectsWithTag("level");
        int realIndex = levels.Length - currentLvlIndex -1;
        GameObject current = levels[realIndex];
        return current;
    }

    public Vector3 getInitLvlPos() 
    {   
        GameObject[] levels;
        levels = GameObject.FindGameObjectsWithTag("level");
        int realIndex = levels.Length - currentLvlIndex -1;
        Transform current = levels[realIndex].transform;
        return current.Find("initPos").transform.position;
    }
} 
