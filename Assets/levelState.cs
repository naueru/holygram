﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelState : MonoBehaviour
{
    // Start is called before the first frame update
    public int toLevelIndex;
    public int initSide;
    public Vector3 initialPosition = new Vector3(2.01f, -1.55f, 1.91f);
    private Vector3 awayPosition = new Vector3(1000, 1000, 1000);

    public void show () {
        gameObject.transform.position = initialPosition;
    }

    public void hide () {
        gameObject.transform.position = awayPosition;
    }
}
